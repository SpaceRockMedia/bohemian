"""Cfg base."""
# Futures
from __future__ import annotations

# Standard Library
import logging
import os

from functools import lru_cache, partial
from ipaddress import IPv4Address
from pathlib import Path
from typing import TYPE_CHECKING, Any

# Third Party
import tomli

from pydantic import (
    AnyHttpUrl,
    BaseSettings,
    EmailStr,
    Field,
    HttpUrl,
    NameEmail,
    PostgresDsn,
    SecretStr,
    validator,
)


if TYPE_CHECKING:
    from pydantic.typing import AnyCallable, AnyClassMethod
    from collections.abc import Callable, MutableMapping

# Library
from bo import __version__
from bo.lib.util import get_toml
from bo.lib.validators import validate_path


_PROJECT_DIR: Path = Path(__file__).parents[3]
print(f"PROJECT_DIR : {_PROJECT_DIR}")
_ENV: str = os.getenv("ENV", "dev")
print(f"ENV: {_ENV}")
_ENVS_DIR: Path = _PROJECT_DIR / "environments" / _ENV.lower()
_ENV_FILE: Path = _ENVS_DIR / ".env"
print(f"ENV_FILE : {_ENV_FILE}")


path_validator: Callable[[AnyCallable], AnyClassMethod] = partial(
    validator, allow_reuse=True
)  # type: ignore


@lru_cache
def get_valid_envs() -> set[str]:
    """Get possible environments based on config."""
    envs: set[str] = set()
    for _, dirs, _ in os.walk(_ENVS_DIR.parent):
        for name in dirs:
            if not name.startswith("_"):
                envs.add(name)
        break

    return envs


@lru_cache
def get_pyproject() -> dict[str, Any]:
    """Get data from pyproject file."""
    with open(_PROJECT_DIR / "pyproject.toml") as f:
        return tomli.loads(f.read())


class AppConfig(BaseSettings):
    """Base config class."""

    class Config:  # type: ignore
        """Settings configurator."""

        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    VERSION: str = __version__

    PROJECT_DIR: Path = _PROJECT_DIR
    _validate_project_dir = path_validator("PROJECT_DIR")(validate_path)  # type: ignore

    SRC_DIR: Path = PROJECT_DIR / "src"
    _validate_src_dir = path_validator("SRC_DIR")(validate_path)  # type: ignore

    _project_toml: MutableMapping[str, Any] = get_toml(
        Path(PROJECT_DIR) / "pyproject.toml"
    )

    PROJECT_NAME: str = "bo"  # get_pyproject()["tool"]["poetry"]["name"]

    APP_DIR: Path = SRC_DIR / PROJECT_NAME
    _validate_app_dir = path_validator("APP_DIR")(validate_path)  # type: ignore

    DOCS_DIR: Path = PROJECT_DIR / "docs"
    _validate_docs_dir = path_validator("DOCS_DIR")(validate_path)  # type: ignore

    ENV: str = "prod"

    @validator("ENV", pre=True)
    def validate_environment(cls, value: str) -> str:  # noqa: B902
        """Make sure ENV is valid."""
        valid_envs = set(get_valid_envs())
        if value in valid_envs:
            return value

        raise ValueError(
            f"Invalid Environment. got [{value}] . "
            f"should be one of [{', '.join(valid_envs)}]"
        )

    ENV_DIR: Path = PROJECT_DIR / "environments" / ENV
    _validate_env_dir = path_validator("ENV_DIR")(validate_path)  # type: ignore

    DEBUG: bool = ENV == "dev" or ENV == "test"

    @validator("DEBUG", pre=True)
    def validate_debug(cls, value: bool, values: dict[str, Any]) -> bool:
        """Ensure DEBUG is set properly."""
        if value and "ENV" in values and values["ENV"] == "PROD":
            raise ValueError(
                "DEBUG can not be set in a PRODUCTION environment. "
                "Please set to `false` or `0` in your environment. "
                f"Got Value: {value}"
            )

        return value

    @property
    def is_prod(self) -> bool:
        """Determine if we are in a production environment."""
        return self.ENV == "PROD" and self.DEBUG is False

    @property
    def is_test(self) -> bool:
        """Determine if we are in a testing environment."""
        return self.ENV == "TEST"

    @property
    def is_dev(self) -> bool:
        """Determine if we are in a development environment."""
        return not self.is_prod and not self.is_test and self.ENV == "DEV"

    ####################################################################################
    # Logging
    LOG_LEVEL: str = "INFO"

    @validator("LOG_LEVEL", pre=True)
    def validate_log_level(cls, value: str | int) -> str:  # noqa: B902
        """Make sure log level is valid."""
        # if passed a number, convert to one of the level strings
        if isinstance(value, int) or value.isnumeric():
            return logging.getLevelName(int(value))

        return str(value).upper()

    # JSON_LOGS: bool = True if os.environ.get("JSON_LOGS", "0") == "1" else False

    # @validator("JSON_LOGS", pre=True)
    # def validate_json_logs(cls, value: "Union[str, int]") -> bool:
    #     """Validate json logs."""
    #     if isinstance(value, int):
    #         return value != 0
    #     elif isinstance(value, str):
    #         return value.lower() == "true"
    #     raise ValueError(
    #         "Invalid value for JSON_LOGS. should be one of 'true' or " "'false'"
    #     )

    ####################################################################################
    # User
    OPEN_REGISTRATION: bool = False


AppConfig.update_forward_refs()
app = AppConfig()


class ServerConfig(BaseSettings):
    class Config:
        env_prefix = "SERVER_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    HOST: HttpUrl | IPv4Address = (
        IPv4Address("0.0.0.0") if app.is_prod else IPv4Address("127.0.0.1")
    )
    PORT: int = 5000
    RELOAD: bool = app.DEBUG
    LOG_LEVEL: str = "CRITICAL"
    JSON_LOGS: bool = True
    # KEEPALIVE: int


server = ServerConfig()


class SecurityConfig(BaseSettings):
    """Security settings for the application."""

    class Config:
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    SECRET_KEY: SecretStr = "CHANGE ME"
    FORCE_SSL: bool = app.ENV == "prod" and not app.DEBUG

    # BACKEND_CORS_ORIGINS is a JSON-formatted list of origins
    # e.g: '["http://localhost", "http://localhost:4200", "http://localhost:3000", \
    # "http://localhost:8080", "http://local.dockertoolbox.tiangolo.com"]'
    BACKEND_CORS_ORIGINS: list[AnyHttpUrl | str] = (
        []
        if app.is_prod
        else [
            f"http://{server.HOST}",
            f"https://{server.HOST}",
            f"http://{server.HOST}:{server.PORT}",
            f"https://{server.HOST}:{server.PORT}",
        ]
    )
    print(f"IS_PROD: {app.is_prod}")
    ALLOWED_HOSTS: list[AnyHttpUrl | str] = (
        [] if app.is_prod else ["*", str(server.HOST)]
    )


security = SecurityConfig()


class APIConfig(BaseSettings):
    """
    API specific configuration.

    Prefix all environment variables with `API_`, e.g., `API_CACHE_EXPIRATION`.
    Attributes
    ----------
    CACHE_EXPIRATION : int
        Default cache key expiration in seconds.
    DEFAULT_PAGINATION_LIMIT : int
        Max records received for collection routes.

    """

    class Config:
        env_prefix = "API_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    URL: str = "/api"

    # 60 minutes * 24 hours * 8 days = 8 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = Field(
        env="ACCESS_TOKEN_EXPIRE_MINUTES",
        default=60 * 24 * 8,
    )

    # CACHE_EXPIRATION: int
    # CREATED_FILTER_DEPENDENCY_KEY: str
    # DB_SESSION_DEPENDENCY_KEY: str
    # DEFAULT_PAGINATION_LIMIT: int
    # DEFAULT_USER_NAME: str
    # HEALTH_PATH: str
    # ID_FILTER_DEPENDENCY_KEY: str
    # LIMIT_OFFSET_DEPENDENCY_KEY: str
    # SECRET_KEY: str
    # UPDATED_FILTER_DEPENDENCY_KEY: str
    # USER_DEPENDENCY_KEY: str


api = APIConfig()


class EmailConfig(BaseSettings):
    """Server email configuration."""

    class Config:
        env_prefix = "EMAIL_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    HOST: str = "example.com"
    # NEW_AUTHOR_SUBJECT: str
    # PORT: int
    # RECIPIENT: str
    EMAIL_ENABLED: bool = False

    TEST_USER: NameEmail | EmailStr = f"test_{app.PROJECT_NAME}@test.{HOST}"
    FROM_EMAIL: NameEmail | EmailStr = TEST_USER
    ADMINS: list[NameEmail | EmailStr] = [
        f"Demo Man <demo@{HOST}>",
        f"Admin <admin@{HOST}>",
    ]


email = EmailConfig()


class OpenAPIConfig(BaseSettings):
    """Configures OpenAPI for the application.
    Prefix all environment variables with `OPENAPI_`, e.g., `OPENAPI_TITLE`.
    Attributes
    ----------
    TITLE : str
        OpenAPI document title.
    VERSION : str
        OpenAPI document version.
    CONTACT_NAME : str
        OpenAPI document contact name.
    CONTACT_EMAIL : str
        OpenAPI document contact email.
    """

    class Config:
        env_prefix = "OPENAPI_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    TITLE: str = "Development OpenAPI Title"
    # VERSION: str
    CONTACT_NAME: str = "Admin"
    CONTACT_EMAIL: NameEmail | EmailStr = email.FROM_EMAIL


openapi = OpenAPIConfig()


class DBConfig(BaseSettings):
    """
    Configures the database for the application.

    Prefix all environment variables with `DB_`, e.g., `DB_URL`.
    Attributes
    ----------
    ECHO : bool
        Enables SQLAlchemy engine logs.
    URL : PostgresDsn
        URL for database connection.

    """

    class Config:
        env_prefix = "DB_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    URL: str | PostgresDsn = "sqlite:///app.sqlite"

    GENERATE_SCHEMAS: bool = False
    ECHO: bool = app.DEBUG
    # ECHO_POOL: bool | Literal["debug"]
    # POOL_MAX_OVERFLOW: int
    # POOL_SIZE: int
    # POOL_TIMEOUT: int


db = DBConfig()


class TemplatingConfig(BaseSettings):
    """Application template config."""

    class Config:
        env_prefix = "TEMPLATES_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    DIR: Path = app.SRC_DIR / "assets" / "templates"
    STATIC_DIR: Path = app.SRC_DIR / "assets" / "static"
    print(f"STATIC_DIR: {STATIC_DIR}")


templating = TemplatingConfig()


class SentryConfig(BaseSettings):
    """Configures sentry for the application.
    Attributes
    ----------
    DSN : str
        The sentry DSN. Set as empty string to disable sentry reporting.
    TRACES_SAMPLE_RATE : float
        % of requests traced by sentry, `0.0` means none, `1.0` means all.
    """

    class Config:
        env_prefix = "SENTRY_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    DSN: HttpUrl | None = None

    # @validator("DSN")
    # def sentry_dsn_can_be_blank(cls, value: str) -> str | None:
    #     """Validate sentry_dsn."""
    #     if value in {"", "none", "false"} or not isinstance(value, str):
    #         return None
    #     return value

    # TRACES_SAMPLE_RATE: float


sentry = SentryConfig()


class UvicornConfig(BaseSettings):
    class Config:
        env_prefix = "UVI_"
        env_file = _ENV_FILE
        env_file_encoding = "utf-8"
        case_sensitive = True
        extra = "ignore"
        frozen = True

    WORKERS_PER_CORE: float = 1.0
    MAX_WORKERS: int = 1
    WEB_CONCURRENCY: int = 1
    GRACEFUL_TIMEOUT: int = 120
    TIMEOUT: int = GRACEFUL_TIMEOUT
    KEEP_ALIVE: int = 5


uvicorn = UvicornConfig()

__all__ = [
    "app",
    "security",
    "api",
    "openapi",
    "db",
    "templating",
    "sentry",
    "server",
    "email",
    "uvicorn",
    "get_valid_envs",
]
