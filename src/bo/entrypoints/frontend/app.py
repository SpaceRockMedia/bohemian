"""Generate the core app."""

# Third Party
from fastapi import FastAPI

# Library
from bo.lib.infra.logging import setup_logger


# placed before cfg import so its configuration logs properly
bo_logger = setup_logger()

# Library
from bo.config import cfg  # noqa
from bo.config.base import Cfg  # noqa


def get_app(cfg: Cfg) -> FastAPI:
    """Application factory."""
    # Third Party
    from loguru import logger

    logger.info(
        f"""
    project name: {cfg.PROJECT_NAME}
         version: {cfg.VERSION}
      bo src dir: {cfg.SRC_DIR}
             env: {cfg.ENV}
           debug: {cfg.DEBUG}
       log level: {cfg.LOG_LEVEL}
      server url: 🚀 http://{cfg.SERVER_HOST}:{cfg.SERVER_PORT}
         api_url: 🏹 http://{cfg.SERVER_HOST}:{cfg.SERVER_PORT}{cfg.API_URL}
         openapi: 📖 http://{cfg.SERVER_HOST}:{cfg.SERVER_PORT}/docs
            docs: 🔖 http://{cfg.SERVER_HOST}:{cfg.SERVER_PORT}/docs
    """
    )

    application = FastAPI(
        title=cfg.PROJECT_NAME,
        version=cfg.VERSION,
        description="My Blog app",
        debug=cfg.DEBUG,
        openapi_url=f"{cfg.API_URL}/openapi.json",
        logger=logger,
    )

    # # Library
    # from bo.lib.infra import databases as db

    # db.init(application, cfg)

    # # Library
    # from bo.lib import middleware

    # middleware.init(application, cfg)

    # Library
    from bo.entrypoints.frontend.main import init

    init(application, cfg)

    return application


app = get_app(cfg)
