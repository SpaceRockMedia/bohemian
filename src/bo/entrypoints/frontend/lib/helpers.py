"""Bohemian utility functions."""

# # Standard Library
# from pathlib import Path

# # Third Party
# import markdown

# # Library
# from bo.config import cfg


# def openfile(filename: str, *, md: bool = True) -> dict[str, str]:
#     """Open a file and read contents, optionally as markdown."""
#     filepath = Path(cfg.TEMPLATES_DIR, "pages", filename)
#     text = ""
#     with open(filepath, encoding="utf-8") as input_file:
#         text = input_file.read()

#     if md or filename.endswith(".md"):
#         text = markdown.markdown(text)

#     return {"text": text}


# def open_markdown(filename: str) -> dict[str, str]:
#     """Open a file as markdown and return the html content."""
#     return openfile(filename, md=True)
