"""Bo frontend module."""
# Third Party
from fastapi import FastAPI

# Library
from bo.config import Cfg


def init(app: "FastAPI", cfg: "Cfg") -> None:
    """Bo app initialization."""
    # Third Party
    # import fastapi_jinja
    # import jinja_partials

    # Library
    # from bo.entrypoints.frontend.routes import client

    # fastapi_jinja.global_init(str(cfg.templates_dir), auto_reload=cfg.debug)
    # partials = Jinja2Templates(str(cfg.templates_dir))
    # partials.env.auto_reload = cfg.debug
    # jinja_partials.register_starlette_extensions(partials)

    # app.include_router(client)

    # app.mount("/static", StaticFiles(directory=cfg.static_dir), name="static")
