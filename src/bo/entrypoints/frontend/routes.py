"""Bo frontend routing."""
# Standard Library

# Standard Library

# # Third Party
# from fastapi import APIRouter
# from fastapi_jinja import template
# from requests import Request

# # Library
# # from bo.entrypoints.frontend.lib.helpers import openfile


# client = APIRouter()


# @client.get("/", response_model=dict[str, str])
# @template("page.html")
# def index(request: "Request") -> dict[str, "Any"]:
#     """Index route."""
#     md = openfile("home.md")
#     return {"request": request, "data": md}


# @client.get("/about", response_model=dict[str, str])
# @template("page.html")
# def about(request: "Request") -> dict[str, "Any"]:
#     """Index route."""
#     data = openfile("about.md")
#     return {"request": request, "data": data}
