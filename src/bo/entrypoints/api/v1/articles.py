"""Describes the endpoint for Articles."""

# Third Party
from fastapi import Depends
from fastapi.responses import JSONResponse
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from sqlmodel import Session
from starlette import status

# Library
from bo.adapters.repository.engine import get_session
from bo.entrypoints.api.model import APIMessage
from bo.modules.article.schema import ArticleCreate, ArticleRead
from bo.modules.article.services import create_article, detail_article, list_articles
from bo.modules.article.unit_of_work import ArticleUnitOfWork


article_api = InferringRouter(
    default_response_class=JSONResponse,
    responses={
        status.HTTP_200_OK: {"model": ArticleRead},
        status.HTTP_404_NOT_FOUND: {"model": APIMessage},
    },
)


def check_results(
    results: list[ArticleRead] | ArticleRead | None,
    code: int,
    detail: str = "No Results Found",
) -> list[...] | ArticleRead | JSONResponse | None:
    if results is None:
        return JSONResponse(
            status_code=code, content={"detail": detail, "data": results}
        )

    return results


@cbv(article_api)
class ArticleApi:
    """API endpoint for Articles."""

    session: Session = Depends(get_session)

    def __init__(self, uow: UnitOfWorkProtocol):
        self.uow = uow or ArticleUnitOfWork(self.session)
        super().__init__()

    @article_api.get("/")
    def index(self) -> list[ArticleRead]:
        """Index route."""
        results = list_articles(self.uow)
        return check_results(results, 404)

    @article_api.get("/{article_id}", responses={})
    def detail(self, article_id: int) -> ArticleRead:
        """Index route."""
        results = detail_article(article_id, self.uow)
        return check_results(results, 404)

    @article_api.post(
        "/",
        status_code=status.HTTP_201_CREATED,
        responses={
            status.HTTP_200_OK: {
                "description": "other defininition",
                "content": {
                    "application/json": {
                        "example": {
                            "title": "Descriptive title"
                        }
                    }
                }
            }
        }
    )
    def create(self, article: ArticleCreate) -> ArticleRead:
        """Create a new article."""
        results = create_article(article, self.uow)
        return check_results(results, 500, "unable to retrieve created article")
