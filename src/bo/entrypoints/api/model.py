# Standard Library
from typing import Any

# Third Party
from pydantic import BaseModel

# Library
from bo.lib.logging import Loggable


class APIModel(BaseModel, Loggable):
    """
    Intended for use as a base class for externally-facing models. Any models that
    inherit from this class will:

    * accept fields using snake_case or camelCase keys
    * use camelCase keys in the generated OpenAPI spec
    * have orm_mode on by default
        * Because of this, FastAPI will automatically attempt to parse returned orm instances into the model

    """

    # class Config(BaseConfig):
    #     """Configuration for the API view class."""
    #
    #     orm_mode = True
    #     allow_population_by_field_name = True
    #     aliases = partial(snake2camel, start_lower=True)


class APIMessage(APIModel):
    """A message sent from the API."""

    detail: str
    results: Any
