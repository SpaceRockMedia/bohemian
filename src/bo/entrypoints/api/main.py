"""Bo main module."""
# Library
from bo import config
from bo.entrypoints.api.app import get_app


app = get_app(config)

app.logger.info(
    f"""
project name: {config.app.PROJECT_NAME}
     version: {config.app.VERSION}
  bo src dir: {config.app.SRC_DIR}
         env: {config.app.ENV}
       debug: {config.app.DEBUG}
   log level: {config.app.LOG_LEVEL}
  server url: 🚀 http://{config.server.HOST}:{config.server.PORT}
     api_url: 🏹 http://{config.server.HOST}:{config.server.PORT}{config.api.URL}
     openapi: 📖 http://{config.server.HOST}:{config.server.PORT}/docs
        docs: 🔖 http://{config.server.HOST}:{config.server.PORT}/redoc
"""
)
