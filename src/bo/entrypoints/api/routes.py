"""Bo frontned routing."""

# Third Party
from fastapi import APIRouter

# Library
from bo.entrypoints.api.v1.articles import article_api


api = APIRouter()
api.include_router(article_api, prefix="/articles", tags=["articles"])


# @router.get("/", response_model=dict[str, str])
# def home() -> dict[str, "Any"]:
#     """Index route."""
#     return {"hello": "world"}
#
#
# @router.get("/about", response_model=dict[str, str])
# def about() -> dict[str, "Any"]:
#     """Index route."""
#     return {"about": "us"}
