"""Generate the core app."""
# Standard Library

# Standard Library
from typing import Any

# Third Party
from starlite import Starlite, State, StaticFilesConfig
from starlite.plugins.tortoise_orm import TortoiseORMPlugin
from tortoise import Tortoise

# Library
from bo.lib.logging import LoguruLoggingConfig


def startup_events(config) -> list[Any]:
    def init_tortoise(state: State) -> None:
        Tortoise.init(db_url=config.DATABASE_URL)
        if config.GENERATE_SCHEMAS:
            Tortoise.generate_schemas()

    return [init_tortoise]


def shutdown_events(config) -> list[Any]:
    def shutdown_tortoise(state: State) -> None:
        Tortoise.close_connections()

    return [shutdown_tortoise]


def get_app(config) -> Starlite:
    """Application factory."""

    return Starlite(
        debug=config.app.DEBUG,
        logging_config=LoguruLoggingConfig(),
        on_startup=startup_events(config),
        on_shutdown=shutdown_events(config),
        plugins=[TortoiseORMPlugin()],
        route_handlers=[],
        static_files_config=[
            StaticFilesConfig(
                path="/static",
                directories=[config.templating.STATIC_DIR],
            ),
        ],
    )
