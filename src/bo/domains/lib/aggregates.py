"""Domain aggregates."""

# Library
from bo.domains.entities import Entity


class Aggregate(Entity):
    """Base aggregate class."""
