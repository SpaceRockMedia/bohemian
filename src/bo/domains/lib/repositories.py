"""Domain repository."""

# Standard Library
from abc import ABCMeta, abstractmethod
from uuid import uuid4

# Third Party
from pydantic import UUID4

# Library
from bo.domains.entities import Entity


class GenericRepository(metaclass=ABCMeta):
    """An interface for a generic repository with CRUD operations."""

    @abstractmethod
    def get_by_id(self, _id: UUID4) -> Entity:
        """Get by instance ID."""
        ...

    @abstractmethod
    def insert(self, entity: Entity) -> Entity:
        """Insert new instance."""
        ...

    @abstractmethod
    def update(self, entity: Entity) -> Entity:
        """Update instance."""
        ...

    @abstractmethod
    def delete(self, entity_id: UUID4) -> Entity:
        """Delete instance."""
        ...

    @staticmethod
    def next_id() -> UUID4:
        """Generate new UUID."""
        return uuid4()
