"""Domain events."""

# Standard Library

# Standard Library
from datetime import datetime
from typing import Any


class DomainEvent:
    """
    Base class for all events in this domain.

    DomainEvents are value objects and all attributes are specified as keyword arguments
    at construction time. There is always a timestamp attribute which gives the event
    creation time in UTC, unless specified. Events are equality comparable.

    """

    def __init__(
        self,
        created: str,
        **kwargs: dict[Any, Any],
    ) -> None:
        """Domain events."""
        self.__dict__["created"] = created or datetime.utcnow().astimezone().isoformat()
        self.__dict__.update(kwargs)

    def __setattr__(self, key: str, value: Any) -> None:
        """Set an object attribute."""
        if hasattr(self, key):
            raise AttributeError(
                f"{self.__class__.__name__} attributes can be added but not modified."
                f"Attribute {key} already exists with value {getattr(self, key)}"
            )
        self.__dict__[key] = value

    def __eq__(self, other: Any) -> bool:
        """Test object equality."""
        if type(self) is not type(other):
            return NotImplemented
        return self.__dict__ == other.__dict__

    def __ne__(self, other: Any) -> bool:
        """Test object inequality."""
        return not (self == other)

    # def __hash__(self) -> int:
    #     """Hashed version of domain event."""
    #     items = self.__dict__.items()
    #     self_list = [type(self)]
    #     ret = itertools.chain(
    #         items,
    #         self_list,
    #     )
    #     return hash(tuple(ret))

    def __repr__(self) -> str:
        """Printable representation of the Event."""
        return (
            self.__class__.__qualname__
            + "("
            + ", ".join("{}={!r}".format(*item) for item in self.__dict__.items())
            + ")"
        )

    def __str__(self) -> str:
        """Object as a string."""
        return self.__repr__()
