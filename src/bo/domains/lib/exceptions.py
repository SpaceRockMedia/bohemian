"""Domain exceptions."""


class DeletedEntityError(Exception):
    """Raised when an attempt is made to use a deleted entity."""


class DomainException(Exception):
    """Domain base exception."""


# class BusinessRuleValidationException(DomainException):
#     """Exception for Business rules."""

#     def __init__(self, rule):
#         self.rule = rule

#     def __str__(self):
#         return str(self.rule)
