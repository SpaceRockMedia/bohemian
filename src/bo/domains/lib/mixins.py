"""Common model objects for `bo`."""

# Third Party
from sqlmodel import Field

# Library
from bo.domains.lib.exceptions import DeletedEntityError


class SoftDeleteMixin:
    """
    Provide the ability to mark as deleted without destroying the record.

    Some description.

    """

    deleted: bool = Field(default=False)
    deleted_date: str

    def _check_not_deleted(self) -> None:
        """Validate entity was marked for deletion."""
        if self.deleted:
            raise DeletedEntityError(f"Attempted to use {repr(self)}")


# class CommentMixin:
#     """Add comment functionality to your models."""

# allow_comments = fields.BooleanField(null=False, default=False)
# comments: fields.ReverseRelation["Comment"]  # noqa: F821


# class BaseModel(Base):
#     """Base model for the app."""
#     __tablename__ = "base"
#
#     id = fields.UUIDField(pk=True)  # noqa: VNE003
#     title = fields.CharField(
#         max_length=128,
#         null=False,
#     )
#     slug = fields.CharField(max_length=64, null=False, unique=True)
#
#     async def get_by_id(self, record_id: int) -> NotImplementedError:
#         """Get record by it's `id`."""
#         raise NotImplementedError()


# @pre_save(Model)
# async def signal_pre_save_Model(
#     sender: "Type[Model]",
#     instance: Model,
#     created: bool,
#     using_db: "Optional[BaseDBAsyncClient]",
#     update_fields: List[str],
# ) -> None:
#     logger.info(
#         "signal_pre_save_Model(",
#         sender,
#         instance,
#         created,
#         using_db,
#         update_fields,
#         ")",
#     )
#     if "title" in update_fields:
#         update_fields["slug"] = slugify(update_fields["title"])
