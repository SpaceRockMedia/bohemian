"""Domain entities."""


# Standard Library
from datetime import datetime, timezone
from typing import Any, NewType, Protocol, TypeVar, runtime_checkable
from uuid import uuid4

# Third Party
from pydantic import UUID4, BaseModel
from tortoise import Model

# Library
from bo.lib.types import StrEnum


EntityVersion = NewType("EntityVersion", int)


def _now() -> datetime:
    """Get the current datetime in ISO 8601 format."""
    return datetime.now(timezone.utc).astimezone()


class VisibilityStatus(StrEnum):
    """Status for model visibility."""

    PUBLIC: str = "public"
    PRIVATE: str = "private"


class ModerationStatus(StrEnum):
    """
    Status string for a moderated object.

    Flow:
    Draft -> Moderation
    Draft -> Published
    Draft -> Archived
    Moderation -> Draft
    Moderation -> Published
    Moderation -> Archived

    """

    DRAFT: str = "draft"
    MODERATION: str = "moderation"
    PUBLISHED: str = "published"
    ARCHIVED: str = "archived"


class Moderated(BaseModel):
    """Representation of a moderated entity."""

    class Config:
        """Entity configurator."""

        use_enum_values = True

    pub_date: datetime = _now()
    status: str = ModerationStatus.DRAFT

    async def archive(self) -> None:
        """Archive the entity."""
        self.status = ModerationStatus.ARCHIVED

    async def publish(self, when: datetime | None = None) -> None:
        """Mark entity as published."""
        self.pub_date: datetime = when or _now()
        self.status = ModerationStatus.PUBLISHED

    async def unpublish(self) -> None:
        """Mark entity as unpublished."""
        self.status = ModerationStatus.ARCHIVED


@runtime_checkable
class Identifiable(Protocol):
    """
    Type for encapsulating ID's for repository objects.

    Defaults to using UUID v4 (uuid4()) to generate the identity.

    """

    _id: UUID4

    @property
    def id(self) -> UUID4:
        """Get unique identifier for this entity."""
        return self._id

    def new_id(self) -> UUID4:
        """Generate a new id value."""
        self._id = uuid4()

        return self._id

    def __hash__(self) -> int:
        """Get object hash."""
        return hash(self._id)

    def __eq__(self, other: Any) -> bool:
        """Test for object equality."""
        return self._id == other._id if isinstance(other, self.__class__) else False

    def __ne__(self, other: Any) -> bool:
        """Test for object inequality."""
        return not self.__eq__(other)

    def __str__(self) -> str:
        """Class to string."""
        return str(self._id)

    def __repr__(self) -> str:
        """Get class instance representation."""
        return f"{self.__class__.__name__}({repr(self._id)})"


class Entity(Model):
    """The base entity class."""


EntityType = TypeVar("EntityType", bound=Entity, covariant=True)
# class AggregateRoot(BusinessRuleValidationMixin, Entity):
#     """Consist of 1+ entities, which spans transaction boundaries."""
