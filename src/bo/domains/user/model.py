"""
User Model Objects.

Users describe the users.

"""

# Third Party
# from sqlmodel import SQLModel


# class User(SQLModel):
#     """The user data model."""

#     # _id = Column(
#     #     UUID(as_uuid=True),
#     #     unique=True,
#     #     nullable=False,
#     #     primary_key=True,
#     #     default=uuid.uuid4(),
#     # )
#     # username = Column(String(128), unique=True, nullable=False)
#     # password_hash = fields.CharField(max_length=128, null=True)
#     # created_at = fields.DatetimeField(auto_now_add=True)
#     # modified_at = fields.DatetimeField(auto_now=True)

#     # class PydanticMeta:
#     #     """Pydantic Meta data for User objects."""
#     #
#     #     computed = ["full_name"]
#     #     exclude = ["password_hash"]


# class Profile(SQLModel):
#     """User profile model."""

#     # first_name = Column(String(50), nullable=False, blank=False)
#     # family_name = Column(String(50), nullable=True, blank=False)

#     #
#     # Relationships
#     #
#     # articles: fields.ReverseRelation["Article"]  # noqa: F821
#     # category = fields.CharField(max_length=30, default="misc")

#     # def full_name(self) -> str:
#     #     """Return the full name."""
#     #     if self.name or self.family_name:
#     #         return f"{self.name or ''} {self.family_name or ''}".strip()
#     #     return self.username
