"""Tag services."""
# Standard Library

# Third Party
# from fastapi import HTTPException
# from sqlmodel import Session, select

# # Library
# from bo.config import cfg
# from bo.domains.tag.model import Tag, TagUpdate
# from bo.lib.infra.databases import get_engine
# from bo.lib.infra.logging import logger
# from bo.lib.util import slugify


# def create_tag(title: str) -> Tag | None:
#     """Create a tag."""
#     with Session(get_engine(cfg)) as sess:
#         tag = Tag(title=title, slug=slugify(title))
#         logger.debug(tag)
#         sess.add(tag)
#         sess.commit()
#         logger.debug(tag)
#         return tag


# def read_tags() -> list[Tag]:
#     """List out tags."""
#     with Session(get_engine(cfg)) as sess:
#         return sess.exec(select(Tag)).all()


# def read_tag(slug: str) -> Tag | None:
#     """Get a single tag."""
#     with Session(get_engine(cfg)) as sess:
#         query = select(Tag).where(Tag.slug == slug)
#         response: Tag = sess.exec(query).first()
#         logger.info(response)

#         return response


# def tag_update(slug: str, tag: TagUpdate = None) -> Tag:
#     """Update a tag."""
#     with Session(get_engine(cfg)) as sess:
#         db_tag = sess.get(Tag, slug)
#         if not db_tag:
#             raise HTTPException(status_code=404, detail="Tag not found")
#         tag_data = tag.dict(exclude_unset=True)

#         return db_tag
