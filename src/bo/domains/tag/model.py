"""
Tag Model Objects.

Tags are a way to categorize content.

"""

# Standard Library
# from dataclasses import dataclass

# # Third Party
# from sqlmodel import Field

# # Library
# from bo.lib.value_objects import ValueObject


# @dataclass
# class TagBase(ValueObject):
#     """Tagging for various content."""

#     slug: str = Field(default=None, primary_key=True)
#     title: str = Field(default=None, max_length=255)


# class Tag(TagBase):
#     pass


# class TagCreate(TagBase):
#     pass


# class TagRead(TagBase):
#     pass


# class TagUpdate(TagBase):
#     pass

# @validator("slug", "title", pre=True, always=True)
# def default_slug(cls, v, values, **kwargs) -> str:
#     """Tag slug validator."""
#     # slug = slugify(v or values["title"])
#     return v

# def __str__(self) -> str:
#     """Return str representation."""
#     return self.slug
#
# def __repr__(self) -> str:
#     """Return the __repr__."""
#     return self.__str__()
