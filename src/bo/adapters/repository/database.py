# Standard Library
from typing import Protocol

# Third Party
from pydantic import UUID4
from sqlmodel import Session, select

# Library
from bo.domains.entities import Entity, EntityType


class RepositoryProtocol(Protocol):
    session = None

    def read(self, entity: Entity, _id: UUID4) -> Entity:
        pass

    def insert(self, entity: Entity) -> Entity:
        pass

    def list(self, entity: Entity) -> list[Entity]:
        pass

    def update(self, _id: UUID4) -> Entity:
        pass

    def delete(self, _id: UUID4) -> None:
        pass


class SqlAlchemyRepository(RepositoryProtocol):
    """SQL Alchemy repository protocol."""

    def __init__(self, session: Session) -> None:
        super().__init__(session)
        self.session = session

    def insert(self, entity: EntityType) -> EntityType:
        with self.session as session:
            session.add(entity)
            session.commit()
            session.refresh(entity)
            return entity

    def detail(self, entity: EntityType, _id: UUID4) -> EntityType:
        with self.session as session:
            results = session.get(entity, _id)
            print("SESSION Results:")
            print(results)
            return results

    def list(self, entity: EntityType) -> list[EntityType]:
        with self.session as session:
            results = session.exec(select(entity))
            print(results)
            all_results = results.all()
            print(all_results)
            return all_results
