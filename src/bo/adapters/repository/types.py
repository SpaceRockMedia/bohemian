# Library
from bo.adapters.repository.filters import BeforeAfter, CollectionFilter, LimitOffset


FilterTypes = BeforeAfter | CollectionFilter | LimitOffset
