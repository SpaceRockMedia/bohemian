"""Bohemian rich_logging."""
# Futures
from __future__ import annotations

# Standard Library
import logging

from typing import Any

# Third Party
from pydantic import BaseModel
from starlite import BaseLoggingConfig, MissingDependencyException
from starlite.types.callable_types import GetLogger

# Library
from bo import config


class LoguruLoggingConfig(BaseLoggingConfig, BaseModel):
    """
    Configuration class for loguru.

    Notes:
        - requires 'loguru' to be installed.

    """

    """Logger factory to use."""
    cache_logger_on_first_use: bool = True
    """Whether to cache the logger configuration and reuse. """

    def configure(self) -> GetLogger:
        """
        Configured logger with the given configuration.

        Returns:
            A 'logging.getLogger' like function.

        """
        try:
            # Third Party
            from loguru import configure, getLogger

            # we now configure loguru
            configure(**self.dict(exclude={"standard_lib_logging_config"}))
            return getLogger
        except ImportError as e:  # pragma: no cover
            raise MissingDependencyException("loguru is not installed") from e


class Loggable:
    """Mixin to provide logging as self.logger.debug(...)."""

    LEVEL = config.app.LOG_LEVEL

    _logger: logging.Logger | None = None
    _opt: dict[str, Any] = {
        "backtrace": True,
        "colors": True,
        "diagnose": True,
        "exception": True,
    }

    @property
    def opt(self) -> dict[str, Any]:
        """Get options."""
        return self._opt

    @opt.setter
    def opt(self, data: dict[str, Any] | None) -> None:
        """Set custom options."""
        if data:
            self._opt.update(data)

    @property
    def log(self) -> logging.Logger:
        """Log something to stdout/stderr."""
        if self._logger is None:
            self._set_logger()

        return self._logger

    def _set_logger(self):
        """Set the local logger."""
        # Third Party
        from loguru import logger

        logger = logger.opt(**self._opt)

        class_logger = logger.bind(task=self.__class__.__name__)

        self._logger = class_logger
