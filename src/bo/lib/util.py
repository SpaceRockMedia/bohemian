"""Bo utility methods."""
# Futures
from __future__ import annotations

# Standard Library
from typing import TYPE_CHECKING, Any

# Third Party
import toml
import yaml

from pydantic import BaseModel, Field, validator


if TYPE_CHECKING:

    # Standard Library
    from collections.abc import MutableMapping
    from pathlib import Path


def slugify(text: str) -> str:
    """Standardize how we slugify strings."""
    # Third Party
    from slugify import slugify as s

    return s(
        text,
        max_length=255,
        word_boundary=True,
        stopwords=["a", "the"],
        regex_pattern=r"[^-a-zA-Z0-9]+",
        replacements=[
            ["|", "or"],
            ["%", "percent"],
            ["&", "and"],
        ],
    )


class Sluggable(BaseModel):
    """Class to convert string to sluggable string."""

    slug: str = Field(max_length=64, null=False, unique=False)
    _slug_default: str | None = None

    @validator("slug")
    def validate_slug(cls, value: str, values: dict[str, Any]) -> str:
        """Make sure the slug field is populated."""
        if isinstance(cls._slug_default, str):
            return slugify(cls._slug_default)
        elif "title" in values:
            return slugify(values["title"])
        elif "name" in values:
            return slugify(values["name"])

        raise ValueError

    def __init__(
        self, slug_default: str | None = None, *args: Any, **kwargs: Any
    ) -> None:
        """Class to convert string to sluggable string."""
        if isinstance(slug_default, str):
            self._slug_default = slugify(slug_default)
        super().__init__(*args, **kwargs)


def pretty_dict(data: dict[str, Any]) -> str:
    """Make a dict all pretty for printing."""
    return yaml.dump(data, default_flow_style=False)


def get_toml(file_path: Path) -> MutableMapping[str, Any]:
    """Load a toml file as a dict."""
    with open(file_path) as f:
        return toml.loads(f.read())
