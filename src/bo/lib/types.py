"""Global app types."""
# Standard Library
import random

from enum import Enum


class StrEnum(str, Enum):
    """Allows for a stringly typed enum to behave as expected."""

    def _generate_next_value_(name, start, count, last_values) -> str:  # noqa
        """Uses the name as the automatic value, rather than an integer See
        https://docs.python.org/3/library/enum.html#using-automatic-values for
        reference."""
        return name

    def choice(self) -> str:
        """
        Choose a random enum element.

        Returns
        -------
        Returns a single choice string.

        """
        return random.choice([i.value for i in list(self)])  # type: ignore


# if __name__ == "__main__":
#     print(Foo.TEST)
#     print(Foo.TEST.value)
#     print(Foo.TEST.name)
#
#     assert Foo.TEST == "test"
#     assert str(Foo.TEST) == "test"
#     assert Foo.TEST.value == "test"
#     assert str(Foo.TEST.value) == "test"
#     assert [thing for thing in Foo] == ["test", "bar", "baz"]
#     assert list(Foo) == ["test", "bar", "baz"]
#     # assert random.choice(Foo) in ["test", "bar", "baz"]
#     assert random.choice(list(Foo)) in ["test", "bar", "baz"]
#     # print(Foo.__members__)


# Standard Library
# from collections.abc import Callable

# # example decorating simple decorator that returns original function
# from typing import Any, TypeVar


# _FuncT = TypeVar("_FuncT", bound=Callable[..., Any])


# def my_decorator(func) -> Callable[[_FuncT], _FuncT]:
#     @functools.wraps(func)
#     def wrapper(*args, **kwargs):
#         return 1

#     return wrapper
