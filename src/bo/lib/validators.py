"""Domain validators."""
# Futures
from __future__ import annotations

# Standard Library
from pathlib import Path

# Third Party
from beartype import beartype


@beartype
def validate_path(file_path: Path) -> Path:
    """Validate a file or directory path exists."""

    if file_path.exists() and (file_path.is_dir() or file_path.is_file()):
        return file_path

    raise FileNotFoundError(
        f"The dir path you passed, does not exist. Got: {file_path}"
    )
