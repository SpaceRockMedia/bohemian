"""Global value objects."""
# Futures
from __future__ import annotations

# Third Party
from pydantic import BaseModel


class ValueObject(BaseModel):
    """Base class for value objects."""

    class Cfg:
        """Internal config."""

        # allow_mutation = False
        frozen = True
