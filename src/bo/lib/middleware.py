"""
App middleware.

Using ASGI generic middleware:
https://fastapi.tiangolo.com/advanced/middleware/#adding-asgi-middlewares

"""
# Futures
from __future__ import annotations

# Standard Library
import time

from typing import TYPE_CHECKING

# Third Party
import secure

from loguru import logger


if TYPE_CHECKING:
    # Standard Library
    from collections.abc import Callable

    # Third Party
    from fastapi import FastAPI, Request, Response

    # Library
    from bo.config import Cfg


def get_secure_headers(cfg: Cfg):  # type: ignore # noqa
    """
    Secure header middleware.

    Parameters
    ----------
    cfg: Cfg
        Configuration object.

    Returns
    -------
    Secure instance object.

    """
    server = secure.Server().set("Secure")

    csp = (
        secure.ContentSecurityPolicy()
        .default_src("'none'")
        .base_uri("'self'")
        .connect_src("'self'", *cfg.ALLOWED_HOSTS)
        .frame_src("'none'")
        .img_src("'self'", *cfg.ALLOWED_HOSTS)
    )

    hsts = (
        secure.StrictTransportSecurity()
        .include_subdomains()
        .preload()
        .max_age(60 * 60 * 24 * 30)  # 30 days
    )

    referrer = secure.ReferrerPolicy().no_referrer()

    permissions_value = (
        secure.PermissionsPolicy().geolocation("self", *cfg.ALLOWED_HOSTS).vibrate()
    )

    cache_value = secure.CacheControl().must_revalidate()

    return secure.Secure(
        server=server,
        csp=csp,
        hsts=hsts,
        referrer=referrer,
        permissions=permissions_value,
        cache=cache_value,
    )


def init(app: FastAPI, cfg: Cfg) -> None:
    # All ENVS
    setup_context(app, cfg)
    setup_timing(app, cfg)

    # PROD ENVS
    # setup_cors(app, cfg)
    # @app.middleware("http")
    # async def set_secure_headers(request, call_next):
    #     """Setup headers via secure library."""
    #     response = await call_next(request)
    #     secure_headers = get_secure_headers(cfg)
    #     secure_headers.framework.fastapi(response)
    #     return response

    # setup_ssl(app, cfg)
    # setup_monitoring(app, cfg)
    setup_proxy(app, cfg)
    # setup_ratelimit(app, cfg)
    # setup_early_data(app, cfg)


def setup_context(app: FastAPI, cfg: Cfg) -> None:
    """
    Add request context.

    https://github.com/tomwojcik/starlette-context

    """
    # # Third Party
    # from starlette_context.middleware import RawContextMiddleware
    #
    # app.add_middleware(
    #     RawContextMiddleware,
    #     plugins=(
    #         plugins.CorrelationIdPlugin(),
    #         plugins.RequestIdPlugin(),
    #     ),
    # )


def setup_timing(app: FastAPI, cfg: Cfg) -> None:
    """Build request time."""

    @app.middleware("http")  # type: ignore
    async def process_time_header(
        request: Request, call_next: Callable  # type: ignore
    ) -> Response:
        """Add process time to headers."""
        start_time = time.time()
        response = await call_next(request)
        process_time = time.time() - start_time
        response.headers["X-Process-Time"] = str(process_time)

        logger.info("Process Time: {} elapsed", process_time)

        return response


def setup_cors(app: FastAPI, cfg: Cfg) -> None:
    """Set all CORS enabled origins."""
    if origins := cfg.BACKEND_CORS_ORIGINS:

        # Third Party
        from starlette.middleware.cors import CORSMiddleware

        app.add_middleware(
            CORSMiddleware,
            allow_origins=[origin.strip() for origin in origins],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )


def setup_ssl(app: FastAPI, cfg: Cfg) -> None:
    """Configure SSL middleware."""
    if cfg.FORCE_SSL:
        # Third Party
        from starlette.middleware.httpsredirect import HTTPSRedirectMiddleware

        app.add_middleware(HTTPSRedirectMiddleware)


def setup_monitoring(app: FastAPI, cfg: Cfg) -> None:
    """Set up monitoring services."""
    # if cfg.is_prod and cfg.sentry_dsn:
    #     # Third Party
    #     import sentry_sdk

    #     from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

    #     sentry_sdk.init(dsn=cfg.sentry_dsn)
    #     app.add_middleware(SentryAsgiMiddleware)


def setup_proxy(app: FastAPI, cfg: Cfg) -> None:
    """Configure the proxy middleware for security."""
    # Third Party

    if cfg.is_prod:
        # Third Party
        from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware

        app.add_middleware(ProxyHeadersMiddleware, trusted_hosts=cfg.ALLOWED_HOSTS)

    # def setup_ratelimit(app: FastAPI, cfg: Cfg) -> None:
    #
    #     # https://github.com/abersheeran/asgi-ratelimit
    #     # Third Party
    #     from ratelimit import RateLimitMiddleware, Rule
    #
    #     async def AUTH_FUNCTION(scope: Scope) -> Tuple[str, str]:
    #         """
    #         Resolve the user's unique identifier and the user's group from ASGI SCOPE.
    #
    #         If there is no user information, it should raise `EmptyInformation`. If there is
    #         no group information, it should return "default".
    #
    #         """
    #         # You must write the logic of this function yourself,
    #         # or use the function in the following document directly.
    #         admins_group = []
    #         user = user_unique_id = 1
    #         if user not in admins_group:
    #             return user_unique_id, "default"
    #
    #     app.add_middleware(
    #         RateLimitMiddleware,
    #         authenticate=AUTH_FUNCTION,
    #         # backend=RedisBackend(),
    #         cfg={
    #             r"^/towns": [Rule(second=1, group="default"), Rule(group="admin")],
    #             r"^/forests": [Rule(minute=1, group="default"), Rule(group="admin")],
    #         },
    #     )


def setup_early_data(app: FastAPI, cfg: Cfg) -> None:
    """
    Configure http early data protection.

    https://github.com/HarrySky/starlette-early-data

    """
    # if cfg.is_prod:
    #     # Third Party
    #     from starlette_early_data import EarlyDataMiddleware
    #
    #     app.add_middleware(EarlyDataMiddleware, deny_all=False)


def setup_compression(app: FastAPI, cfg: Cfg) -> None:
    """App compression."""
    if cfg.is_prod:
        # Third Party
        from starlette.middleware.gzip import GZipMiddleware

        app.add_middleware(GZipMiddleware, minimum_size=1000)


def setup_security(app: FastAPI, cfg: Cfg) -> None:
    """Set up app compression."""
    if cfg.is_prod:
        # Third Party
        from starlette.middleware.trustedhost import TrustedHostMiddleware

        app.add_middleware(TrustedHostMiddleware, allowed_hosts=cfg.ALLOWED_HOSTS)
