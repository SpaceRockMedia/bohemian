# Library
from bo.entrypoints.api.model import APIModel


class ArticleSchema(APIModel):
    """Base schema for Article objects."""

    title: str
    description: str
    body: str
    use_markdown: bool


class ArticleCreate(ArticleSchema):
    """Required fields for creating a new article."""


class ArticleRead(ArticleSchema):
    """Required fields for reading an article."""

    id: int


class ArticleNone(APIModel):
    pass
