"""
User Model objects.

The model should strictly define the data as it is tied to its 'source'. Generally this
is a database, but a model could be attached to a csv file, or an external service.

"""

# Library
from bo.domains.entities import Entity


class Article(Entity, table=True):
    """Article model."""

    id: int | None = Field(default=None, primary_key=True)
    title: str = Field(
        title="Title",
        description="info",
        max_length=128,
        min_length=3,
    )

    #
    # Content
    #
    description: str = Field()
    body: str = Field()
    use_markdown: bool = Field(default=True)

    # author_id: UUID4 = fields.UUIDField()
    # author: fields.ForeignKeyRelation["User"] = fields.ForeignKeyField(  # noqa
    #     "models.User",
    #     related_name="articles",
    #     on_delete=fields.RESTRICT,
    # )

    #
    # Moderation
    # published = fields.BooleanField(default=False, null=False)
    # published_date: datetime = fields.DatetimeField(null=True, auto_now_add=True)
    # published_by: fields.ForeignKeyRelation["User"] = fields.ForeignKeyField(  # noqa
    #     "models.User", on_delete=fields.RESTRICT, related_name="articles"
    # )

    #
    # Relationships
    #
    # tags: fields.ManyToManyRelation["Tag"] = fields.ManyToManyField(
    #     "models.Tag",
    #     through="article_tags",
    #     related_name="articles",
    #     on_delete=fields.RESTRICT,
    #     backward_key="article",
    #     forward_key="tag,",
    # )

    # async def add_tag(self, tag: "Tag") -> AsyncGenerator[bool, None]:
    #     """Add tag to article."""
    #     if tag not in self.tags:
    #         await self.tags.add(tag)
    #         yield True
    #     yield False
    #
    # async def remove_tag(self, tag: "Tag") -> AsyncGenerator[bool, None]:
    #     """Remove tag from article."""
    #     if tag in self.tags:
    #         await self.tags.remove(tag)
    #         yield True
    #     yield False


# class Article(ArticleBase, table=True):
#     """Article with identity."""
#
#     id: int | None = Field(default=None, primary_key=True)
#
#
# class ArticleCreate(ArticleBase):
#     """Article create."""
#
#
# class ArticleRead(Article):
#     """Article read."""


#
# class ArticleUpdate(Article):
#     """Article update."""
#
#
# class ArticleDelete(Article):
#     """Article delete."""
