# Third Party

# Library
from bo.modules.article.entity import Article
from bo.modules.article.schema import ArticleCreate, ArticleRead
from bo.modules.article.unit_of_work import UnitOfWorkProtocol


def create_article(article: ArticleCreate, uow: UnitOfWorkProtocol) -> ArticleRead:
    """Create a new article."""

    return uow.articles.insert(
        Article(
            title=article.title,
            description=article.description,
            body=article.body,
            use_markdown=article.use_markdown,
        )
    )


def detail_article(_id: int, uow: UnitOfWorkProtocol) -> ArticleRead | None:
    """Read a specific article."""
    results = uow.articles.detail(Article, _id)
    print("READ Article:")
    print(results)
    return results


def list_articles(uow: UnitOfWorkProtocol) -> list[ArticleRead]:
    """List all articles."""
    return uow.articles.list(Article)
