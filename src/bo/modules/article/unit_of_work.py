# Futures
from __future__ import annotations

# Standard Library
from typing import ContextManager, TypeVar

# Third Party
from sqlmodel import Session

# Library
from bo.adapters.repository.database import SqlAlchemyRepository


class UnitOfWork(ContextManager):
    """Base for all unit-of-work protocols."""

    def __init__(self, session: Session):
        self.session = session

    def __enter__(self) -> UnitOfWorkProtocol:
        return super().__enter__()

    def __exit__(self, *args) -> None:
        super().__exit__(*args)
        self.session.close()
        self._commit()

    def _commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()


UnitOfWorkProtocol = TypeVar("UnitOfWorkProtocol", bound=UnitOfWork, covariant=True)


class ArticleUnitOfWork(UnitOfWork):
    """Article based UOW."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.articles = SqlAlchemyRepository(self.session)

    def __enter__(self) -> UnitOfWorkProtocol:
        self.articles = SqlAlchemyRepository(self.session)
        return super().__enter__()
