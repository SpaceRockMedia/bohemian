# type: ignore
"""Gunicorn config."""

# Standard Library
import json
import multiprocessing
import os

# Library
from bo.config import cfg
from bo.lib.infra.logging import setup_logger


setup_logger()


workers_per_core_str = cfg.uvi.WORKERS_PER_CORE
use_max_workers = int(cfg.uvi.MAX_WORKERS)
web_concurrency_str = cfg.uvi.WEB_CONCURRENCY

host = cfg.server.HOST
port = cfg.server.PORT
bind_env = os.getenv("BIND", None)
use_loglevel = cfg.LOG_LEVEL
use_bind = bind_env or f"{host}:{port}"
cores = multiprocessing.cpu_count()
workers_per_core = float(workers_per_core_str)
default_web_concurrency = workers_per_core * cores
if web_concurrency_str:
    web_concurrency = int(web_concurrency_str)
    assert web_concurrency > 0  # noqa
else:
    web_concurrency = max(int(default_web_concurrency), 2)
    if use_max_workers:
        web_concurrency = min(web_concurrency, use_max_workers)
use_accesslog = None
use_errorlog = None
graceful_timeout_str = cfg.uvi.GRACEFUL_TIMEOUT
timeout_str = cfg.uvi.TIMEOUT
keepalive_str = cfg.uvi.KEEP_ALIVE


# Gunicorn config variables
loglevel = use_loglevel
workers = web_concurrency
bind = use_bind
errorlog = use_errorlog
worker_tmp_dir = "/dev/shm"
accesslog = use_accesslog
graceful_timeout = int(graceful_timeout_str)
timeout = int(timeout_str)
keepalive = int(keepalive_str)


# For debugging and testing
log_data = {
    "loglevel": loglevel,
    "workers": workers,
    "bind": bind,
    "graceful_timeout": graceful_timeout,
    "timeout": timeout,
    "keepalive": keepalive,
    "errorlog": errorlog,
    "accesslog": accesslog,
    # Additional, non-gunicorn variables
    "workers_per_core": workers_per_core,
    "use_max_workers": use_max_workers,
    "host": host,
    "port": port,
}

logger.info(json.dumps(log_data))
