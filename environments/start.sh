#!/usr/bin/env sh

set -ex

echo "start"
export env="prod"
/venv/bin/gunicorn \
  -k uvicorn.workers.UvicornWorker \
  -c /app/environments/gunicorn_conf.py \
  "${APP_MODULE}"
