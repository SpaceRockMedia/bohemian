#!/usr/bin/env sh

set -ex

echo "start-reload"
. /app/environments/dev/.env
ENV=dev

echo "env       : ${ENV}"
echo "APP_MODULE: ${APP_MODULE}"

poetry install --no-ansi --no-interaction

uvicorn \
  --reload \
  --host "${SERVER_HOST:-127.0.0.1}" \
  --port "${SERVER_PORT:-5000}" \
  --log-level "$(echo ${LOG_LEVEL:-debug} | tr '[:upper:]' '[:lower:]')" \
  "${APP_MODULE}"
  # lets do API first. do only backend
#  "${app_module:-'src/bo/entrypoints/frontend/app:app'}"
