#!/bin/bash

set -o errexit
set -o nounset

#if [ -z "${POSTGRES_USER}" ]; then
#    base_postgres_image_default_user='postgres'
#    export POSTGRES_USER="${base_postgres_image_default_user}"
#fi
#export DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}"

#postgres_ready() {
#    # Check that postgres is up and running on port `5432`:
#    dockerize -wait 'tcp://postgres:5432' -timeout 5s
#}
#until postgres_ready; do
#  >&2 echo 'Waiting for PostgreSQL to become available...'
#  sleep 1
#done
#>&2 echo 'PostgreSQL is available'

# run migrations
#alembic upgrade head

# create initial data in db
#python /app/scripts/initial_data.py
