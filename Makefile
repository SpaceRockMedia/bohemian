ENV=dev
SERVER_HOST=127.0.0.1
SERVER_PORT=5000
NODE_PORT=3000
LOG_LEVEL=debug
APP_MODULE="bo.entrypoints.api.main:app"
CI_REGISTRY=registry.gitlab.com
CI_PROJECT_NAMESPACE=autoferrit
CI_PROJECT_NAME=bohemian

# registry.gitlab.com/spacerockmedia/bohemian
IMAGE_FULL_NAME=${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}

# registry.gitlab.com/spacerockmedia/bohemian:buildbase
APP_BUILD_BASE=${IMAGE_FULL_NAME}:buildbase

# registry.gitlab.com/spacerockmedia/bohemian:dev
APP_DEV=${IMAGE_FULL_NAME}:dev
# registry.gitlab.com/spacerockmedia/bohemian:dev-env
APP_DEV_ENV=${IMAGE_FULL_NAME}:dev-env

# registry.gitlab.com/spacerockmedia/bohemian:latest
APP_PROD_LATEST=${IMAGE_FULL_NAME}:latest
# registry.gitlab.com/spacerockmedia/bohemian:env
APP_PROD_ENV=${IMAGE_FULL_NAME}:env


###
# Building
# Base:
# - buildbase
# Dev:
# - bohemian:dev-env
# - bohemian:dev
# Prod:
# - bohemian:env
# - bohemian:latest

###

.DEFAULT_GOAL := help # Sets default action to be help

# start of Python section
define PRINT_HELP_PYSCRIPT
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
	# if the line has a command and a comment start with
	#   two pound signs, add it to the output
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		output.append("· %-20s - %s" % (target, help.capitalize()))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
output = ["Commands               Description"] + output
output.insert(1, ('--'*len(output[0])))
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section

help: ## This help dialog
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

env:
	. ./environments/dev/.env

######
# app

start: env ## run app using uvicorn
	uvicorn \
	  --reload \
	  --host ${SERVER_HOST} \
	  --port ${SERVER_PORT} \
	  --log-level ${SERVER_LOG_LEVEL} \
	  ${APP_MODULE}

######

######
# docker

DC = docker compose # --env-file /dev/null

build-prod-env: ## Build the production environment
	${DC} -f environments/docker-compose.yml \
		-f environments/prod/docker-compose.env.yml \
		build app

build-prod: build-prod-env ## Build the production image
	${DC} -f environments/docker-compose.yml \
		-f environments/prod/docker-compose.yml \
		build app

build-dev-env: ## Build development environment
	${DC} -f environments/dev/docker-compose.env.yml \
		build --no-cache

build-dev: ## Build the development image
	${DC} -f environments/docker-compose.yml \
		build --no-cache

build-dev-env-cached: ## Build development environment
	${DC} -f environments/docker-compose.env.yml \
		build

build: build-dev-env build-dev ## Shortcut to build both development environments

build-all: build-prod build-dev ## build both development and production images

up: ## Start the development docker image using docker-compose
	${DC} -f environments/docker-compose.yml up -d \
	&& ${DC} -f environments/docker-compose.yml logs -f

down: ## Start the development docker image using docker-compose
	${DC} -f environments/docker-compose.yml down

push-dev-env: ## Push the production environment to the remote repository
	docker push "${APP_DEV_ENV}"

push-dev: ## Push the production image to the remote repository
	docker push "${APP_DEV}"

push-prod-env: ## Push the production environment to the remote repository
	docker push "${APP_PROD_ENV}"

push-prod: ## Push the production image to the remote repository
	docker push "${APP_PROD_LATEST}"

docs-docker:
	${DC} run --rm app \
	sphinx-build docs/ docs/_build/html/

######

######
# Linting

bandit: ## Run bandit on source
	pre-commit run bandit

flake8: ## Check for programmer and syntax errors
	pre-commit run flake8

flake8-all: ## Check for programmer and syntax errors
	pre-commit run --all flake8

mypy: ## Type check code with mypy
	pre-commit run mypy

mypy-all: ## Type check code with mypy
	pre-commit run --all mypy

proselint: ## Check text for grammer
	pre-commit run proselint

pydocstyle: ## Check document styles
	pre-commit run pydocstyle

pydocstyle-all: ## Check document styles
	pre-commit run --all pydocstyle

shellcheck: ## Validate bash scripts with shellchesk.
	pre-commit run shellcheck

.PHONY: lint
lint: bandit flake8 mypy proselint pydocstyle shellcheck ## Run all lint checks

.PHONY: lint-all
lint-all: bandit flake8-all mypy-all proselint pydocstyle-all ## Run all lint checks

######

######
# Format

autoflake: ## run autoflake on changed files
	pre-commit run autoflake

autoflake-all: ## Run autoflake on all files
	pre-commit run -all autoflake

black: ## run black on changed files
	pre-commit run black

black-all: ## Run black on code base
	pre-commit run --all black

docformatter: ## run `docformatter` against docstrings in edited files. BETA
	pre-commit run docformatter

isort: ## run isort on changed files
	pre-commit run isort

isort-all: ## Run isort on all files
	pre-commit run --all src

pyupgrade: ## Run pyupgrade
	pre-commit run pyupgrade

pyupgrade-all: ## Run pyupgrade
	pre-commit run --all pyupgrade

yamlfix-all: ## Check yaml file syntax
	find . \
		! -path "./node_modules/*" \
		! -path "./.venv/*" \
		-type f -name '*.yml' -o -name '*.yaml' \
		| tr '\n' ' ' \
	    | xargs -I {} bash -c "yamlfix {}"

.PHONY: imports
imports: autoflake isort ## run autoflake and isort on changed files

.PHONY: fmt
fmt: imports black docformatter pyupgrade yamlfix ## Format code with autoflake, isort, and black

fmt-all: autoflake-all isort-all black-all docformatter-all pyupgrade-all yamlfix-all ## Format all code with autoflake, isort, and black
######

######
# Testing and Docs

pytest: ## Run tests
	pytest && xdg-open htmlcov/index.html


docs: ## Generate docs
	sphinx-build docs/ docs/_build/html/ \
	&& xdg-open docs/_build/html/index.html

test: pytest docs

######

######
# Misc

deps: build-dev-env ## rebuild the packages requirements.txt
	pre-commit run prod-requirements
	pre-commit run dev-requirements

commit:
	pre-commit install && pre-commit run --all

######

######
# Cleanup

.PHONY: clean
clean: clean-pyc clean-pycache clean-patchfiles clean-backupfiles clean-generated clean-testfiles clean-buildfiles clean-mypyfiles

.PHONY: clean-pyc
clean-pyc: ## clean pyc files
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +

.PHONY: clean-pycache
clean-pycache:
	find . -name __pycache__ -exec rm -rf {} +

.PHONY: clean-patchfiles
clean-patchfiles:
	find . -name '*.orig' -exec rm -f {} +
	find . -name '*.rej' -exec rm -f {} +

.PHONY: clean-backupfiles
clean-backupfiles:
	find . -name '*~' -exec rm -f {} +
	find . -name '*.bak' -exec rm -f {} +
	find . -name '*.swp' -exec rm -f {} +
	find . -name '*.swo' -exec rm -f {} +

.PHONY: clean-generated
clean-generated:
	find . -name '.DS_Store' -exec rm -f {} +
	rm -rf Sphinx.egg-info/
	rm -rf dist/
	rm -rf doc/_build/
	rm -f sphinx/pycode/*.pickle
	rm -f utils/*3.py*
	rm -f utils/regression_test.js

.PHONY: clean-testfiles
clean-testfiles:
	rm -rf tests/.coverage
	rm -rf tests/build
	rm -rf .tox/
	rm -rf .cache/
	rm -rf .pytest_cache/

.PHONY: clean-buildfiles
clean-buildfiles:
	rm -rf build

.PHONY: clean-mypyfiles
clean-mypyfiles:
	find . -name '.mypy_cache' -exec rm -rf {} +
######

# https://stackoverflow.com/a/6273809/1826109
%:
	@:
