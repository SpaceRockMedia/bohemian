"""Bohemian utils test."""
# # Standard Library
# import string
#
# from random import random
# from typing import Dict
#
# # Library
# from bo.config import get_config
#
#
# config = get_config()
#
#
# def random_lower_string() -> str:
#     """Make a random string."""
#     return "".join(random.choices(string.ascii_lowercase, k=32))
#
#
# def get_superuser_token_headers(test_client) -> Dict[str, str]:
#     """Get superuser token headers."""
#     login_data = {
#         "username": config.FIRST_SUPERUSER,
#         "password": config.FIRST_SUPERUSER_PASSWORD,
#     }
#     r = test_client.post(f"{config.API_URL}/login/access-token", data=login_data)
#     tokens = r.json()
#     a_token = tokens["access_token"]
#
#     return {"Authorization": f"Bearer {a_token}"}
