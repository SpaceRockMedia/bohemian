"""Bohemian main test."""

# Third Party
from fastapi import FastAPI

# Library
from bo.config import Cfg
from bo.entrypoints.api.app import get_app


def test_make_app(config: Cfg) -> None:
    """Get app instance with test config."""
    assert isinstance(get_app(config), FastAPI)


# def test_read_root(api_client: TestClient) -> None:
#     """Read root path."""
#     response = api_client.get("/")
#     # data = response.json()
#     body = response.text
#     assert response.status_code == 200
#     assert "Hello" in body
#     # assert f"{config.project_name} at" in body


# def test_get_config(config):
#     """Test config."""
#     assert "testing" == config.ENVIRONMENT.lower()
#     assert "*" in config.ALLOWED_HOSTS
