"""Test for Article Entities."""
# Third Party
import pytest

from hypothesis import given, strategies

# Library
from bo.modules.article import Article


@pytest.fixture(scope="module")
def article() -> Article:
    """Return an article."""
    return Article(
        title="Test article",
        description="Test article description",
        body="Test article body",
        use_markdown=False,
    )


@given(strategies.builds(Article))
def test_can_create_article(self, article: Article) -> None:
    """Test can_create_article."""
    assert isinstance(article, Article)
    assert 2 < len(article.title) <= 128
    assert isinstance(article.use_markdown, bool)
