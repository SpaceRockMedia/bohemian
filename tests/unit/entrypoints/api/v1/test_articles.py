# Third Party

# Third Party
from starlette.testclient import TestClient

# Library
from bo.config import cfg
from bo.entrypoints.api.routes import router


client = TestClient(router)


def test_index_title() -> None:
    # print(client.base_url)
    response = client.get(f"{cfg.API_URL}/articles")
    print(response.url)
    assert response.status_code == 200
    assert response.json() == {"title": "hello articles."}
