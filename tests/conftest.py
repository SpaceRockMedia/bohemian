"""Pytest configuration."""

pytest_plugins = ("tests.fixtures",)

# @pytest.fixture(scope="module")
# def superuser_token_headers(test_client):
#     """Superuser token headers."""
#     return get_superuser_token_headers(test_client)
#
#
# @pytest.fixture(scope="module")
# def normal_user_token_headers(test_client):
#     """Normal user token headers."""
#     return authentication_token_from_email(test_client, config.EMAIL_TEST_USER)

# Standard Library
from collections import abc
from typing import TYPE_CHECKING

# Third Party
import pytest

from starlite.testing import TestClient


if TYPE_CHECKING:
    # Third Party
    from starlite import Starlite


@pytest.fixture()
def app() -> "Starlite":
    """
    Always use this `app` fixture and never do `from app.main import app` inside a test
    module.

    We need to delay import of the `app.main` module
    until as late as possible to ensure we can mock everything necessary before
    the application instance is constructed.
    Returns:
        The application instance.

    """
    # don't want main imported until everything patched.
    # Library
    from bo.entrypoints.api.main import (
        app as the_app,  # pylint: disable=import-outside-toplevel
    )

    return the_app


@pytest.fixture()
def client(
    app: "Starlite",
) -> abc.Iterator[TestClient]:  # pylint: disable=redefined-outer-name
    """
    Client instance attached to app.

    Args:
        app: The app for testing.
    Returns:
        Test client instance.

    """
    with TestClient(app=app) as c:
        yield c
