"""Bohemian fixtures."""
# Third Party
import pytest

from fastapi import FastAPI
from starlette.testclient import TestClient

# Library
from bo.config import Cfg, get_config
from bo.entrypoints.api.app import get_app


@pytest.fixture(scope="session", autouse=True)
def config() -> Cfg:
    """Cfg fixture."""
    return get_config(
        env="test",
        DEBUG=True,
    )


@pytest.fixture(scope="session")
def app(config: Cfg) -> FastAPI:
    """App fixture for testing."""
    return get_app(config)


# @pytest.fixture(scope="session")
# def api_client(app: FastAPI) -> TestClient:
#     """Make test client."""
#     return TestClient(app)
