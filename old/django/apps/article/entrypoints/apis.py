# Standard Library
import logging

# Third Party
from ninja import Query
from ninja_extra import api_controller
from pydantic import UUID4

# Library
from bo.entrypoints.apis import BaseApiController, route

from ..domain.entity import ArticleCreate, ArticleDetail
from ..domain.value_objects import Filters
from ..services import handlers
from ..services.unit_of_work import ArticleUnitOfWork


logger = logging.getLogger(__name__)


@api_controller("/articles", tags=["articles"])
class ApiController(BaseApiController):
    """Article api handler."""

    def __init__(self):
        self.uow = ArticleUnitOfWork()
        self.handlers = handlers
        super().__init__(self.uow)

    @route.post("/", response=ArticleDetail)
    def create(self, entity: ArticleCreate):
        """Create an entity."""
        return self.handlers.create(entity, self.uow)

    @route.get("/", response=list[ArticleDetail])
    def list_articles(self, filters: Filters = Query(...)):
        """List all articles."""
        logger.info("Testing info")
        logger.debug("Testing Debug")
        logger.warning("Testing Warning")
        return self.handlers.read_many(self.uow, filters)

    @route.get("/{article_id}", response=ArticleDetail)
    def detail(self, article_id: UUID4):
        """View an articles details."""
        return self.handlers.detail(article_id, self.uow)

    @route.put("/{int:article_id}", response=ArticleDetail)
    def put_article(self, article_id: UUID4, payload: ArticleCreate):
        """Update a whole article object, in place."""
        return self.handlers.put(article_id, payload, self.uow)

    @route.patch("/{int:article_id}", response=ArticleDetail)
    def patch_article(self, article_id: UUID4, payload: ArticleCreate):
        """Update fields of an article."""
        return self.handlers.patch(article_id, payload, self.uow)

    @route.delete("/{int:article_id}")
    def delete_article(self, article_id: UUID4):
        """Delete an article."""
        return self.handlers.delete(article_id, self.uow)
