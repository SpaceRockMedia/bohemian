# Standard Library
from datetime import datetime

# Third Party
from pydantic import UUID4

# Library
from bo.domain.entity import (
    Entity,
    EntityCreate,
    EntityDetail,
    EntityList,
    EntityUpdate,
)


class ArticleEntity(Entity):
    """Article scheme for details."""

    title: str
    slug: str
    created: datetime
    modified: datetime
    content: str


class ArticleCreate(EntityCreate):
    """Article scheme for creation."""

    title: str
    content: str


class ArticleDetail(EntityDetail):
    """Article scheme for details."""

    id: UUID4


class ArticleList(EntityList):
    """Article schema for listing articles."""


class ArticleUpdate(EntityUpdate):
    """Article update."""

    title: str
    content: str
