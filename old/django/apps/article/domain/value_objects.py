# Library
from bo.domain.value_objects import ValueObject


class Filters(ValueObject):
    """Filter articles."""

    limit: int = 10
    offset: int = 0
    query: str | None = None
