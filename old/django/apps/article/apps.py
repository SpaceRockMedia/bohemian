# Third Party
from django.apps import AppConfig


class ArticleConfig(AppConfig):
    """Article configuration."""

    name: str = "apps.article"
    verbose_name: str = "Article"
