# Futures
from __future__ import annotations

# Library
from bo.services.unit_of_work import UnitOfWork

from ..adapters.repositories import Article


class ArticleUnitOfWork(UnitOfWork):
    """Article based UOW."""

    def __init__(self, repo: Article = None):
        self.repo: Article
        super().__init__(repo or Article)
