# Standard Library
import logging

# Third Party
from django.db.models import QuerySet
from django.shortcuts import get_object_or_404
from pydantic import UUID4

from ..adapters.repositories import Article
from ..domain.entity import ArticleCreate
from ..domain.value_objects import Filters
from ..services.unit_of_work import ArticleUnitOfWork


logger = logging.getLogger(__name__)


def create(article: ArticleCreate, uow: ArticleUnitOfWork):
    """Create article service."""
    return uow.repo.objects.create(**article.dict())


def detail(article_id: UUID4, uow: ArticleUnitOfWork) -> Article:
    """Service to retrieve article details."""
    return get_object_or_404(uow.repo, pk=article_id)


def read_many(uow: ArticleUnitOfWork, filters: Filters) -> QuerySet[Article]:
    """Get a list of articles."""
    logger.debug("READ Many")
    return uow.repo.objects.all()[filters.offset : filters.limit + filters.offset]


def put(article_id: UUID4, payload: ArticleCreate, uow: ArticleUnitOfWork) -> Article:
    """Update an article wholly in one pass."""
    article = detail(article_id, uow)
    update_fields = []
    for attr, value in payload.dict().items():
        if hasattr(article, attr):
            setattr(article, attr, value)
            update_fields.append(attr)

    uow.repo.objects.update()
    article.save(update_fields=update_fields)

    return article


def patch(article_id: UUID4, payload: ArticleCreate, uow: ArticleUnitOfWork) -> Article:
    """Update a part of an article."""
    return put(article_id, payload, uow)


def delete(article_id: UUID4, uow: ArticleUnitOfWork):
    """Delete an article."""
    article = detail(article_id, uow)
    article.delete()
    return {"success": True}
