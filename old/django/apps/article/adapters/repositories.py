# Standard Library
from typing import final

# Third Party
from django.db import models

# Library
from bo.adapters.repositories import NodeRepository


@final
class Article(NodeRepository):
    """Article model."""

    content = models.TextField()

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"
        app_label = "article"
