# Third Party
from django.contrib import admin

from .adapters.repositories import Article


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    """Admin panel for ``Article`` model."""
