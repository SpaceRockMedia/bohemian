# Standard Library
from typing import get_type_hints

# Third Party
from ninja_extra.controllers import Route
from pydantic import UUID4

# Library
from bo.domain.entity import EntityCreate, EntityDetail
from bo.services.unit_of_work import UnitOfWork


# https://github.com/dmontagu/fastapi-utils/blob/master/fastapi_utils/inferring_router.py
class Router(Route):
    """Custom router adding support to set response based on return type."""

    def __init__(self, *args, **kwargs):
        if kwargs.get("response") is None:
            kwargs["response"] = get_type_hints(self.view_func).get("return")
        super().__init__(*args, **kwargs)


route = Router


class BaseApiController:
    """Base API controller for an API entrypoint."""

    def __init__(self, uow: UnitOfWork):
        self.uow = uow

    @route.post(response=EntityDetail)
    def create(self, entity: EntityCreate):
        """Create an entity."""
        raise NotImplementedError()

    @route.get()
    def detail(self, entity_id: int | UUID4):
        raise NotImplementedError()
