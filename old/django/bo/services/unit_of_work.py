# Futures
from __future__ import annotations

# Library
from bo.adapters.repositories import Repository


class UnitOfWork:
    """Base for all unit-of-work protocols."""

    def __init__(self, repo: Repository):
        self.repo: Repository = repo
