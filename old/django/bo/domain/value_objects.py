# Third Party
from pydantic import BaseModel


class ValueObject(BaseModel):
    """Base value object for all objects."""
