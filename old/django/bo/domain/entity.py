# Third Party
from ninja import Schema


class Entity(Schema):
    """Base entity class for all entities in a domain."""


class EntityCreate(Entity):
    """Entity creation."""


class EntityDetail(Entity):
    """Entity details."""


class EntityList(Entity):
    """Entity list."""


class EntityUpdate(Entity):
    """Entity update."""
