# Standard Library
import uuid

# Third Party
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.fields import AutoSlugField
from django_extensions.db.models import TimeStampedModel


class UUIDField(models.UUIDField):
    def __init__(self, *args, **kwargs):
        kwargs["max_length"] = 36
        super().__init__(*args, **kwargs)


class TitleSlugMixin(models.Model):
    """Protocol for any object with title and slug."""

    title = models.CharField(_("title"), max_length=128)
    slug = AutoSlugField(
        _("slug"), populate_from="title", overwrite=True, allow_duplicates=False
    )

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return self.slug


class TimeStampedMixin(TimeStampedModel):
    """Protocol for any object with a timestamp."""

    class Meta:
        abstract = True


class IdentifiableMixin(models.Model):
    """Model with an id."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    class Meta:
        abstract = True
