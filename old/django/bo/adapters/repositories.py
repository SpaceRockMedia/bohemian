# Standard Library

# Third Party
from django.db import models

# Library
from bo.adapters.mixins import IdentifiableMixin, TimeStampedMixin, TitleSlugMixin


class Repository(models.Model):
    """Base model to define any kind of persistent data storage."""

    class Meta:
        abstract = True


class DjangoRepository(IdentifiableMixin, TimeStampedMixin, Repository):
    """Base model class for django orm models."""

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return f"{self.__class__.__name__}(id={self.id})"


class NodeRepository(TitleSlugMixin, DjangoRepository):
    """Core model for most user facing content."""

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return f"{self.__class__.__name__}({self.slug}, pk={self.id})"
