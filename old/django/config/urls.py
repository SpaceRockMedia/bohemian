"""
api URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))

"""


# Standard Library

# Third Party
from djadmin2.site import djadmin2_site
from django.conf import settings
from django.contrib.admindocs import urls as admindocs_urls
from django.urls import include, path
from django.views.generic import TemplateView
from ninja_extra import NinjaExtraAPI

# Library
from apps.article.entrypoints.apis import ApiController as ArticleApi


api = NinjaExtraAPI()

api.register_controllers(ArticleApi)

urlpatterns = [
    # django-admin
    # path("admin/", admin.site.urls),
    path("admin2/", include(djadmin2_site.urls)),
    path("admin/doc/", include(admindocs_urls)),
    # Text and xml static files:
    path(
        "robots.txt",
        TemplateView.as_view(
            template_name="txt/robots.txt",
            content_type="text/plain",
        ),
    ),
    path(
        "humans.txt",
        TemplateView.as_view(
            template_name="txt/humans.txt",
            content_type="text/plain",
        ),
    ),
    # ninja api
    path("api/", api.urls),
]

if settings.DEBUG and settings.ENVIRON == "dev":
    # Third Party
    from django.conf.urls.static import static  # noqa: WPS433

    urlpatterns = [
        path("__debug__/", include("debug_toolbar.urls")),
        *urlpatterns,
        # Serving media files in development only:
        *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),
    ]
