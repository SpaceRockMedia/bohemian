"""
bo version number.

Versioning takes a primary parts. Similar to the NODE project
# Major versions may break backwards compatability. Where changes are noted in the
changelog. This is where deprecated items are removed.
# Minor versions try their best to keep API compatability, but sometimes changes are
needed. Though most changes are implementation details, therefor should be able to
keep API compatability. So they make no guarantees to non-breaking changes. Most of the time,
this is where deprecation warnings will appear. But will not be implemented until the next major release.
# Patch versions are guaranteed to be a drop in replacement and should never have any
issues for upgrades.

Outside the above, there are a few other notes.
* ODD MINOR versions, refer to "development" releases. Not guaranteed to be stable
prior to the next EVEN release. Examples of odd development releases
* * 0.1.0, 1.1.0, 1.3.0, 1.3.2, 2.1.0, 2.5.1, 2.5.13
* Where EVEN MINOR versions represent a stable release.
* * 0.2.0, 1.2.0, 1.4.0, 1.4.2, 2.0.0, 2.4.1, 2.6.13

Named releases, which is when 'alpha' or 'beta' are included in the version number,
primarily represent the current state of the upcoming api changes.
* Alpha: means that the API may be subject to change prior to release. So no
guarantees are expected.
* Beta: Means that major API changes are mostly stable, but some may still surface.
It also means no "NEW" changes will be implemented, only improve upon those that will be
included in the next release.
* RC: An API freeze is in place and only bug fixes and improvements that do not
change the API will be implemented.

"""
__version__: str = "0.1.0-Alpha"
__version_info__ = tuple(
    int(num) if num.isdigit() else num
    for num in __version__.replace("-", ".", 1).split(".")
)
