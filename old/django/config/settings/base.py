"""
Django settings for api project.

Generated by 'django-admin startproject' using Django 4.1.2.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.1/ref/settings/

"""

# Standard Library

# Standard Library
from pathlib import Path
from typing import Any

# Third Party
from environ import environ


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR: Path = Path(__file__).resolve().parent.parent.parent.parent

env = environ.Env()

ENV_DIR = BASE_DIR / "environments"

# read the defaults for the env
environ.Env.read_env(ENV_DIR / ".env")

ENVIRON = env("ENV", default="prod")

# read env specific settings
environ.Env.read_env(str(Path(ENV_DIR / ENVIRON / ".env")), overwrite=True)
# load custom local envs
if (local_env := BASE_DIR / ".env").exists():
    environ.Env.read_env(local_env, overwrite=True)


# SECURITY
# ------------------------------------------------------------------------------

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY: str = env.str("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG: bool = env.bool("DEBUG", default=ENVIRON == "dev")

# Password validation
# https://docs.djangoproject.com/en/4.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS: list[dict[str, str]] = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# CORE
# ------------------------------------------------------------------------------
ALLOWED_HOSTS: list[str] = env.list("ALLOWED_HOSTS", default=[])
WSGI_APPLICATION: str = "config.wsgi.application"
ASGI_APPLICATION: str = "config.asgi.application"

# Application definition

INSTALLED_APPS: list[str] = [
    # "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "ninja_extra",
    "apps.article.ArticleConfig",
]

MIDDLEWARE: list[str] = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
]

# FRONTEND
# ------------------------------------------------------------------------------


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.1/howto/static-files/

STATIC_URL: str = "static/"
STATIC_DIR = BASE_DIR / "src" / "static"

MEDIA_URL = "/media/"
MEDIA_ROOT = BASE_DIR / "src" / "static" / "media"

ROOT_URLCONF: str = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]


# DATABASES
# ------------------------------------------------------------------------------

DATABASES: dict[str, Any] = {
    "default": env.db(default=f"sqlite:///{BASE_DIR}/app.sqlite")
}

# Default primary key field type
# https://docs.djangoproject.com/en/4.1/ref/settings/#default-auto-field

# DEFAULT_AUTO_FIELD: str = "django.db.models.BigAutoField"


# INTERNATIONALIZATION
# ------------------------------------------------------------------------------

# https://docs.djangoproject.com/en/4.1/topics/i18n/

LANGUAGE_CODE: str = "en-us"

TIME_ZONE: str = "UTC"

USE_I18N: bool = True

USE_TZ: bool = True

# LOGGING
# ------------------------------------------------------------------------------
LOG_LEVEL = env.str("LOG_LEVEL", "INFO")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "rich": {"datefmt": "[%X]"},
    },
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse",
        },
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue",
        },
    },
    "handlers": {
        # "console": {
        #     "class": "logging.StreamHandler",
        # },
        "console": {
            # https://github.com/adamghill/django-rich-logging/issues/7
            # "class": "django_rich_logging.logging.DjangoRequestHandler",
            "class": "rich.logging.RichHandler",
            "formatter": "rich",
            "level": "DEBUG",
            "filters": ["require_debug_true"],
        },
    },
    "loggers": {
        "": {
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "django.server": {
            "handlers": ["console"],
            "level": "DEBUG",
        },
        "django.request": {"level": "CRITICAL"},
    },
    # "root": {
    #     "handlers": ["console"],
    #     "level": "DEBUG",
    # },
}


# LOGURU
# ------------------------------------------------------------------------------

# DJANGO_LOGGING_MIDDLEWARE = {
#     "DEFAULT_FORMAT": True,
#     "MESSAGE_FORMAT": "<b><green>{time}</green> <cyan>{message}</cyan></b>",
#     "LOG_USER": False,
# }
# INSTALLED_APPS.append("django_loguru")
# MIDDLEWARE.append("django_loguru.middleware.DjangoLoguruMiddleware")

# RICH
# ------------------------------------------------------------------------------


# Django Ninja Extra
# ------------------------------------------------------------------------------
NINJA_EXTRA = {
    "PAGINATION_CLASS": "ninja_extra.pagination.PageNumberPaginationExtra",
    "PAGINATION_PER_PAGE": 10,
    "INJECTOR_MODULES": [],
    "THROTTLE_CLASSES": [
        "ninja_extra.throttling.AnonRateThrottle",
        "ninja_extra.throttling.UserRateThrottle",
    ],
    "THROTTLE_RATES": {
        "user": "1000/day",
        "anon": "100/day",
    },
    "NUM_PROXIES": None,
}

# Django Admin 2
# ------------------------------------------------------------------------------
INSTALLED_APPS = [
    "djadmin2",
    "djadmin2.themes.djadmin2theme_bootstrap3",  # for the default theme
    "rest_framework",  # for the browsable API templates
    *INSTALLED_APPS,
]

REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": 10,
}
ADMIN2_THEME_DIRECTORY = "djadmin2theme_bootstrap3"
