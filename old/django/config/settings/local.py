"""Local settings config."""
# lint:ignore E266

# Standard Library
import socket

# Library
from config.settings.base import *  # noqa  # NOSONAR


# CACHES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}

# debug-toolbar
# ------------------------------------------------------------------------------

# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel"],
    "SHOW_TEMPLATE_CONTEXT": True,
}
INTERNAL_IPS = [
    "localhost",
    "127.0.0.1",
    "10.0.2.2",
]

# docker support
if env("USE_DOCKER") == "yes":
    hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
    INTERNAL_IPS = [ip[: ip.rfind(".")] + ".1" for ip in ips]


# should be earliest in list
INSTALLED_APPS.insert(0, "debug_toolbar")
MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")

# django-extensions
# ------------------------------------------------------------------------------
INSTALLED_APPS.append("django_extensions")
RUNSERVERPLUS_SERVER_ADDRESS_PORT = "0.0.0.0:5000"
